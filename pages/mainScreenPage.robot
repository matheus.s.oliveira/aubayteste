*** Settings ***
Resource    ../core/basePage.robot
Variables    ../locators/mainScreen.py


*** Keywords ***

Go to Products Session
    Click    ${productsButton}


Go to Cart Session
    Click    ${cartButton}


Close Frame
    Select Frame    ${frame}
    Click       ${closeFrame}    10
    Unselect Frame
