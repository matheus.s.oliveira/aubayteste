*** Settings ***
Resource    ../core/basePage.robot
Variables    ../locators/mainScreen.py
Variables    ../locators/products.py

Resource    ../datapool/products/combinations.robot

*** Keywords ***

Add "${combination}"
    Set Global Variable    ${GlobalCombination}    ${combination}
    ${prices}    Set Variable    0
    FOR    ${element}    IN    @{${combination}}
        Write    ${searchInput}    ${element}
        Click    ${searchButton}

        ${valuesProducts}=    Get Price Products    ${priceProducts}
        Click    ${addCartButton}
        Click    ${confirmAddProduct}
        ${prices}=    Evaluate    int(${prices}) + int(${valuesProducts})
        Set Global Variable    ${GlobalPrices}    ${prices}
    END




