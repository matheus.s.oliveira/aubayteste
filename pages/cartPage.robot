*** Settings ***
Resource    ../core/basePage.robot
Variables    ../locators/mainScreen.py
Variables    ../locators/products.py

Resource    ../datapool/products/combinations.robot

*** Keywords ***

Check All Added Products
    FOR    ${element}    IN    @{${GlobalCombination}}
        Elements Present in Cart   productsName    ${element}
    END

Check Sum Prices of Products
    ${pricesOnCart}    Set Variable    0
    FOR    ${element}    IN    @{${GlobalCombination}}
        ${locatorValuesOnCart}=    Elements Present in Cart   productsValue    ${element}
        ${valuesProductsOnCart}=    Get Price Products    ${locatorValuesOnCart}
        ${pricesOnCart}=    Evaluate    int(${pricesOnCart}) + int(${valuesProductsOnCart})
    END
        Assert Integers    ${Globalprices}    ${pricesOnCart}