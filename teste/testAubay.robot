*** Settings ***
Resource    ../core/basePage.robot
Resource    ../pages/mainScreenPage.robot
Resource    ../pages/productsPage.robot
Resource    ../pages/cartPage.robot



Test Setup    Open Application    ${Browser}
Test Teardown      End Application

*** Variables ***

${Browser}             firefox

*** Test Cases ***

Add productson Cart
    Go to Products Session
    Add "List1"
    Go to Cart Session
    Check All Added Products
    Check Sum Prices of Products