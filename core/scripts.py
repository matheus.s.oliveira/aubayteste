def validationReturn(option: str, name: str) -> str:
  match option:
    case "productsName":
        return "//tr[contains(@id, 'product')]//a[contains(text(), '" + name + "')]"
    case "productsValue":
        return "//tr[contains(@id, 'product')]//a[contains(text(), '" + name + "')]//ancestor::tr//p[contains(text(), 'Rs.')]"
