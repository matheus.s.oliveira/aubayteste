*** Settings ***
Library            scripts.py
Library            SeleniumLibrary
Library            String
Library            Collections

Variables           ../datapool/environment/URL.py

*** Variables ***
${timeoutSec}    80
${GlobalPrices}    0


*** Keywords ***
Open Application
    [Arguments]    ${Browser}
    [Documentation]   Generic test setup to Open Browser
    Run Keyword If    '${Browser}'=='firefox'     Open Browser    ${URL}    ${Browser}    options=add_argument("--ignore-certificate-errors")
    ...     ELSE IF   '${Browser}'=='chrome'    Open Browser    ${URL}    ${Browser}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--disable-infobars")
    ...     ELSE      Log       Open Application went wrong.
    Maximize Browser Window

End Application
    [Documentation]   Finish Application and reset Global Variables to Suite Tests
    Set Global Variable    ${GlobalCombination}    List
    Set Global Variable    ${GlobalPrices}    0
    Close Browser

Assert Integers
    [Arguments]     ${first-integer}    ${second-integer}
    [Documentation]    Compare two intergers.
    Should Be Equal As Integers    ${first-integer}    ${second-integer}
    
Click
    [Arguments]     ${locator}    ${timeout}=${timeoutSec}
    [Documentation]    Waits for an element identified by ``locator`` and Click on it.
    ...
    ...    *Input Arguments:*
    ...    | *Name*  | *Description* |
    ...    | locator | xpath of the element to click |
    ...    | timeout | OPTIONAL: time to wait element on screen |
    
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}     ${timeout}
    SeleniumLibrary.Click Element  ${locator}

Write
    [Arguments]    ${locator}    ${text}    ${timeout}=${timeoutSec}
    [Documentation]    Waits for an element identified by ``locator`` and writes ``text`` on it.
    ...
    ...    *Input Arguments:*
    ...    | *Name*   | *Description* |
    ...    | locator  | xpath of the element to write in |
    ...    | text     | text to write |
 
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    ${timeout}
    SeleniumLibrary.Clear Element Text    ${locator}
    SeleniumLibrary.Input Text    ${locator}    ${text}


Elements Present in Cart
    [Documentation]    Checking for elements in Cart.
    ...
    ...    *Input Arguments:*
    ...    | *Name*          | *Description* |
    ...    | option          | productsName OR productsVlue |
    ...    | cartElement     | Element for search |
    ... 
    [Arguments]    ${option}    ${cartElement}       ${timeout}=${timeoutSec}
    ${locator}=    scripts.validationReturn     ${option}    ${cartElement}
    RETURN    ${locator}

Split strings
    [Arguments]    ${text}    ${separator}    ${index}
    [Documentation]    Split strings.
    ...
    ...    *Input Arguments:*
    ...    | *Name*    | *Description* |
    ...    | text      | string to be splited |
    ...    | separator | delimiter string |
    ...    | index     | index to get value|
    ${list}=    String.Split String          ${text}    ${separator}
    ${splited}=    Collections.Get From List    ${list}    ${index}
    RETURN    ${splited}

Get Price Products
    [Arguments]    ${locator}
    [Documentation]    Get value of products.
    ...
    ...    *Input Arguments:*
    ...    | *Name*           | *Description* |
    ...    |     locator      | Locator to Get value of products |

    ${valueString}=    Get Text    ${locator}
    ${valueProduct}=    Split strings    ${valueString}    ${SPACE}    -1
    RETURN    ${valueProduct}