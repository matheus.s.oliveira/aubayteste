searchInput = "//input[@id = 'search_product']"
searchButton = "//button[@id = 'submit_search']"
addCartButton = "//a[contains(text(), 'Add to cart')]"


confirmAddProduct = "//button[contains(@class, 'btn-success') and contains(text(), 'Continue Shopping')]"
priceProducts = "//div[contains(@class, 'productinfo text-center')]//h2[contains(text(), 'Rs.')]"